#ifndef LOG_HPP
#define LOG_HPP

#include <string>

void LOG_D(const std::string& msg);

#endif
