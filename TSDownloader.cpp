#include <chrono>
#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <thread>

#ifdef __cplusplus
extern "C" {
#include <openssl/evp.h>
#include <openssl/err.h>
#endif

#ifdef __cplusplus
}
#endif

#include "FileWrapper/FileWrapper.hpp"

#include "log.hpp"
#include "httpurl.hpp"
#include "TSDownloader.hpp"

using namespace std;
using namespace std::chrono_literals;
using namespace boost::filesystem;


typedef sp::FileWrapper FW;

class TSDownloader::TSDownloaderImpl
{
public:
    TSDownloaderImpl(std::string const& url, std::string const& name);
    ~TSDownloaderImpl();

    int download();

private:
    int downloadM3U8();
    bool isM3U8Valid();
    void parseM3U8File();
    void downloadKeyFile();
    void getURLPrefix();
    std::string getFilePath(const std::string& fileName);
    void adjustM3U8File();
    int generateDownloadList();
    void generateMergeList();
    int downloadTSStream();
    int decryptTSFiles();
    int concatTS();
    void removeAllTSFiles();

private:
    HTTPURL uri;
    std::string baseName;
    std::string tsFileName;
    std::string m3u8URL;
    std::string m3u8File;
    std::string mergeFile;
    std::string tsDownloadListFile;
    std::string urlPrefix;
    std::string keyUrl;
    std::string encryptMethod;
    std::vector<std::string> keyVec;
    bool downloadComplete;
    bool encrypted;
};

TSDownloader::TSDownloaderImpl::TSDownloaderImpl(const string& url, const string& name)
    : uri{url}, baseName{name}, tsFileName{name + ".mp4"},
      m3u8URL{url},
      m3u8File{name + ".m3u8"}, mergeFile{name + ".ml"},
      tsDownloadListFile{name + ".dl"}, downloadComplete{false},
      encrypted{false}
{
    auto dot_pos = m3u8URL.find_last_of('.');
    if (m3u8URL.substr(dot_pos) == ".ts") {
        auto slash_pos = m3u8URL.find_last_of('/');
        if (slash_pos != string::npos) {
            m3u8URL.replace(slash_pos + 1, m3u8URL.length() - slash_pos - 1, "index.m3u8");
            uri = HTTPURL(m3u8URL);
        }
    }

    string domain = uri.protocol + "://" + uri.domain;
    if (false == uri.port.empty()) {
        domain += ":" + uri.port;
    }

    keyVec.push_back(domain);
}

TSDownloader::TSDownloaderImpl::~TSDownloaderImpl()
{
    if (downloadComplete) {
        remove(m3u8File);
        remove(tsDownloadListFile);
        remove(mergeFile);
        if (exists("key.key")) {
            remove("key.key");
        }
    }
}

int TSDownloader::TSDownloaderImpl::download()
{
    LOG_D("Start to download M3U8 file");
    if (0 != downloadM3U8()) return -1;
    LOG_D("M3U8 file download complete");

    LOG_D("Start to parse M3U8 file");

    parseM3U8File();

    string msg{"TS encrypted: "};
    msg += encrypted ? "yes" : "no";
    msg += ", method: ";
    msg += encrypted ? encryptMethod : "(null)";
    msg += ", key url: ";
    msg += encrypted ? keyUrl : "(null)";
    LOG_D(msg);

    LOG_D("Start to download key file if any");
    downloadKeyFile();

    LOG_D("Start to download all ts files");
    int retryTimes = 0;
    while (retryTimes++ < 5) {
        if (0 != generateDownloadList()) {
            cout << "Failed to generate download list" << endl;
            return -1;
        }
        LOG_D("Download list file generated");

        LOG_D("Start to download ts files");
        if (0 == downloadTSStream()) {
            break;
        }
    }

    LOG_D("TS files download Complete");

    LOG_D("Start to generate merge list");
    generateMergeList();
    LOG_D("Merge list generated");

    LOG_D("Start to decrypt ts files if they are encrypted");
    if (0 != decryptTSFiles()) {
        LOG_D("Failed to decrypt ts file");
        return -1;
    }

    LOG_D("Start to merge ts files");
    if (0 != concatTS()) {
        LOG_D("Merge failed");
        return -1;
    }

    LOG_D("Merge complete. Delete all ts files");
    removeAllTSFiles();

    downloadComplete = true;

    return 0;
}

int TSDownloader::TSDownloaderImpl::downloadM3U8()
{
    string targetPath = uri.path;

    while (1) {
        if (!exists(m3u8File)) {
            m3u8URL = uri.protocol + "://" + uri.domain;
            if (false == uri.port.empty()) {
                m3u8URL += ":" + uri.port;
            }

            if ('/' == targetPath[0]) {
                m3u8URL += targetPath;
            }
            else {
                m3u8URL += path(uri.path).parent_path().string() + '/' + targetPath;
            }

            LOG_D("M3U8 URL: " + m3u8URL);
            keyVec.push_back(path(m3u8URL).parent_path().string());
            string downloadCmd = "aria2c -c \"" + m3u8URL + "\" --out=\"" + m3u8File + "\"";
            if (0 != system(downloadCmd.c_str())) {
                LOG_D("Try original url: " + uri.original_url);
                string downCmd = "aria2c -c \"" + uri.original_url +"\" -o " + m3u8File;
                if (0 != system(downCmd.c_str())) {
                    return -1;
                }
            }
        }

        if (true == isM3U8Valid()) {
            getURLPrefix();
            adjustM3U8File();
            break;
        }
        else {
            targetPath = getFilePath(m3u8File);
            remove(m3u8File);
        }
    }

    return 0;
}

bool TSDownloader::TSDownloaderImpl::isM3U8Valid()
{
    FW f(m3u8File);
    for (auto& line : f.getContents()) {
        if ('#' != line[0]) {
            auto ext = path(line).extension();
            if (ext == ".ts") {
                return true;
            }
            if (ext == ".m3u8") {
                return false;
            }
            break;
        }
    }

    return true;
}

void TSDownloader::TSDownloaderImpl::parseM3U8File()
{
    FW f(m3u8File);
    for (auto& line : f.getContents()) {
        if ('#' != line[0]) break;

        auto idx = line.find("EXT-X-KEY");
        if (idx != string::npos) {
            encrypted = true;
            auto midx = line.find("METHOD=");
            auto comma = line.find(',', midx);
            encryptMethod = line.substr(midx + 7, comma - midx - 7);
            auto keyIdx = line.find("URI=", comma);
            keyUrl = line.substr(keyIdx + 5, line.length() - keyIdx - 5 - 1);
        }
    }

    if (keyUrl.empty()) {
        keyUrl = "key.key";
    }
}

void TSDownloader::TSDownloaderImpl::downloadKeyFile()
{
    bool downloaded = false;
    if (encrypted) {
        while (!downloaded) {
            for (auto& prefix : keyVec) {
                string key_url = (0 == keyUrl.compare(0, 4, "http") ? "" : prefix + "/") + keyUrl;
                LOG_D("Key Url: " + key_url);
                string cmd = "aria2c -c \"" + key_url + "\" --out=key.key";
                if (0 == system(cmd.c_str())) {
                    downloaded = true;
                    break;
                }
            }
        }
    }
}

void TSDownloader::TSDownloaderImpl::getURLPrefix()
{
    string filePath;

    if (exists(tsDownloadListFile)) {
        filePath = getFilePath(tsDownloadListFile);
    }
    else {
        filePath = getFilePath(m3u8File);
    }

    string dir = path(filePath).parent_path().string();
    if (dir.empty()) {
        urlPrefix = path(m3u8URL).parent_path().string();
    }
    else if (0 == dir.compare(0, 7, "http://")
        || 0 == dir.compare(0, 8, "https://")) {
        urlPrefix = dir;
    }
    else {
        string parentPath = path(uri.path).parent_path().string();

        urlPrefix = uri.protocol + "://" + uri.domain;
        if (false == uri.port.empty()) {
            urlPrefix += ":" + uri.port;
        }
        urlPrefix += parentPath;

        size_t sameCount = 0;
        for (size_t i = 0; i < parentPath.size()&& i < dir.size(); ++i) {
            if (parentPath[i] == dir[i]) {
                ++sameCount;
            }
            else {
                break;
            }
        }
        if (sameCount < dir.size()) {
            urlPrefix += dir.substr(sameCount);
        }
    }

    LOG_D("URL Prefix: " + urlPrefix);
}

string TSDownloader::TSDownloaderImpl::getFilePath(const string& fileName)
{
    string targetPath;
    FW f(fileName);
    for (auto& line : f.getContents()) {
        if ('#' != line[0]) {
            targetPath = line;
            break;
        }
    }

    return targetPath;
}

void TSDownloader::TSDownloaderImpl::adjustM3U8File()
{
    FW f(m3u8File, sp::FileCloseAction::OUTPUT);

    f.applyFunctionToContents([](const string& line) -> string {
            if ('#' == line[0]) return line;

            return path(line).filename().string();
            });
}

int TSDownloader::TSDownloaderImpl::generateDownloadList()
{
    int ret = 0;

    if (exists(m3u8File)) {
        if (is_regular_file(m3u8File)) {
            FW f(m3u8File);
            FW::File contents;
            int fileNum = 0;
            for (auto& line : f.getContents()) {
                if ('#' == line[0]) continue;
                stringstream ss;
                ss << baseName << "_" << setfill('0') << setw(5)
                   << fileNum << setw(0) << ".ts";
                if (!exists(ss.str()) || exists(ss.str() + ".aria2")) {
                    contents.push_back(urlPrefix + "/" + line);
                    contents.push_back("    out=" + ss.str());
                }
                ++fileNum;
            }

            FW fl(contents.begin(), contents.end());
            fl.setFilename(tsDownloadListFile);
            fl.setClosingAction(sp::FileCloseAction::OUTPUT);
        }
        else if (is_directory(m3u8File)) {
            ret = - 1;
            cout << m3u8File << " is directory" << endl;
        }
        else {
            ret = - 1;
            cout << m3u8File << " exists, but neither regular file or directory" << endl;
        }
    }
    else {
        ret = -1;
        cout << m3u8File << " is not exists" << endl;
    }

    return ret;
}

int TSDownloader::TSDownloaderImpl::downloadTSStream()
{
    string downloadCmd = "aria2c -i \"" + tsDownloadListFile + "\" -j 8";
    return system(downloadCmd.c_str());
}

static void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int TSDownloader::TSDownloaderImpl::decryptTSFiles()
{
    int ret = 0;
    if (encrypted) {
        unsigned char* cdata = new unsigned char[4 * 1024 * 1024];
        unsigned char* ddata = new unsigned char[4 * 1024 * 1024];
        unsigned char keyBuf[16] = {0,};
        std::ifstream key{"key.key", std::ifstream::in | std::ifstream::binary};
        key.read(reinterpret_cast<char*>(keyBuf), 16);
        key.close();

        FW f(mergeFile);
        for (auto& line : f.getContents()) {
            if (line.length() == 0) continue;
            if ('#' == line[0]) continue;

            LOG_D("decrypt " + line);
            std::ifstream ifile{line, std::ifstream::in | std::ifstream::binary};
            int size = ifile.seekg(0, std::ifstream::end).tellg();
            ifile.seekg(0, std::ifstream::beg);

            unsigned char iv[16] = {0,};
            int len = 0;
            int clen = size - 16;
            int dlen = 0;

            ifile.read(reinterpret_cast<char*>(iv), 16);
            ifile.read(reinterpret_cast<char*>(cdata), clen);
            ifile.close();

            EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
            if (!ctx) {
                handleErrors();
            }

            if (1 != EVP_DecryptInit_ex(ctx, EVP_aes_128_cbc(), NULL, keyBuf, iv)) {
                handleErrors();
            }

            if (1 != EVP_DecryptUpdate(ctx, ddata, &len, cdata, clen)) {
                handleErrors();
            }

            dlen = len;

            if(1 != EVP_DecryptFinal_ex(ctx, ddata + len, &len)) {
                handleErrors();
            }

            dlen += len;
            EVP_CIPHER_CTX_free(ctx);

            std::ofstream ofile(line, std::ofstream::trunc | std::ofstream::binary);
            ofile.write(reinterpret_cast<char*>(ddata), dlen);
            ofile.close();
        }
        delete[] ddata;
        delete[] cdata;
    }

    return ret;
}

int TSDownloader::TSDownloaderImpl::concatTS()
{
    string concatCmd = "ffmpeg -i " + mergeFile + " -c copy " + tsFileName; 
    return system(concatCmd.c_str());
}

void TSDownloader::TSDownloaderImpl::removeAllTSFiles()
{ 
    FW f(mergeFile);

    for (auto& line : f.getContents()) {
        if ('#' == line[0]) continue;
        try {
            remove(line);
        }
        catch (filesystem_error& fe) {
            cout << fe.what() << endl;
        }
    }
}

void TSDownloader::TSDownloaderImpl::generateMergeList()
{
    if (!exists(mergeFile)) {
        FW f(m3u8File);
        FW mf(f, sp::FileCloseAction::OUTPUT);
        mf.setFilename(mergeFile);

        int fileNum = 0;
        bool keyLineRemoved = false;
        mf.applyFunctionToContents(
                [&fileNum, &keyLineRemoved, this](auto&& line) {
                    stringstream ss;
                    ss << baseName << "_" << setfill('0') << setw(5)
                       << fileNum << setw(0) << ".ts";
                    bool fileExists = exists(ss.str());

                    if ('#' == line[0]) {
                        if (encrypted) {
                            if (false == keyLineRemoved && line.find("EXT-X-KEY") != string::npos) {
                                line = "";
                                keyLineRemoved = true;
                            }
                        }

                        return fileExists ? line : "";
                    }
                    else {
                        ++fileNum;
                    }

                    return fileExists ? ss.str() : "";
                });
        mf.appendLine(f.getLastLine());
    }
}

TSDownloader::TSDownloader(const std::string& url, const std::string& name)
{
    impl = std::make_unique<TSDownloaderImpl>(url, name);
}

TSDownloader::~TSDownloader()
{

}

int TSDownloader::download()
{
    return impl->download();
}
