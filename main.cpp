#include <boost/filesystem.hpp>
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <string>
#include <unistd.h>
#include <vector>

#include "httpurl.hpp"
#include "TSDownloader.hpp"
#include "log.hpp"

using namespace std;
using StrVec = std::vector<std::string>;

bool tools_check()
{
    StrVec tools = { "ffmpeg", "aria2c" };
    int out_saved = dup(STDOUT_FILENO);

    close(STDOUT_FILENO);
    for (auto const& tool : tools) {
        auto cmd = std::string("which ") + tool;
        int ret = system(cmd.c_str());
        if (ret) {
            LOG_D(tool + " not installed");
            return false;
        }
    }

    dup2(out_saved, STDOUT_FILENO);
    return true;
}

int main(int argc, char* argv[])
{
    if (argc < 3) {
        cout << "Usage: \n"
             << "   TSDownload url file_name" << endl;
        return -1;
    }

    if (!tools_check()) {
        return -1;
    }

    TSDownloader{argv[1], argv[2]}.download();

    return 0;
}
