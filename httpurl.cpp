#include <iostream>
#include <boost/regex.hpp>

#include "httpurl.hpp"

HTTPURL::HTTPURL(std::string url) : original_url{url}
{
    /** {protocol}:://{domain}[:port][path][?query]
     *     1             2       3      4      5
     */
    boost::regex re{"(http|https)://([^/ :]+):?([^/ ]*)(/?[^ #?]*)\\x3f?([^ #]*)#?([^ ]*)"};
    boost::cmatch match;
    if (regex_match(url.c_str(), match, re)) {
        protocol = std::string(match[1].first, match[1].second);
        domain = std::string(match[2].first, match[2].second);
        port = std::string(match[3].first, match[3].second);
        path = std::string(match[4].first, match[4].second);
    }
}
