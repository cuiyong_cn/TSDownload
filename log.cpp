#include <iostream>
#include "log.hpp"

using namespace std;

string dashLine(int len)
{
    string line;
    for (int i = 0; i < len; ++i) {
        line += '-';
    }
    return line;
}

void LOG_D(const string& msg)
{
#ifdef DEBUG
    int len = msg.length() + 8;
    string dline = dashLine(len);
    cout << dline << endl;
    cout << "|   " << msg << "   |" << endl;
    cout << dline << endl;
#endif
}
