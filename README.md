# TSDownload

#### Description
下载TS流, 只需要m3u8文件的http地址即可. 玩具级别的工具.

1. 需要提前安装aria2c(用于下载m3u8文件), ffmpeg(用于将所有的分段ts合并成一个文件)
2. 需要boost filesystem库, (其实不需要也可以, 不过正在玩boost, 所以加上来练习)
3. 需要cypto库, 用于对加密ts流的解码
4. 编译好后, 将工具放入path路径就可以了

其实直接用ffmpeg就能将m3u8的流拉取下来, 只不过ffmpeg是顺序下载的, 所以用aria2c多线程快速下载下来, 然后在本地合并.

其实对于网速非常快的来说, 这个速度提升不大. 但是网速慢的来说, 可以利用aria2c的续传能力, 省时间. 因为ffmpeg中途中断后,
就不得不重新开始

这个程序其实拿python,shell等脚本语言实现会更简单.

#### Software Architecture

#### Installation

1. git clone
2. make
3. 将编译后的文件放到自己指定的路径就可以了

#### Instructions

#### Contribution
