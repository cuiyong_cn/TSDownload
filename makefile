ifneq (${MAKE_HOST},x86_64-pc-cygwin)
    RM := del
    CC := x86_64-w64-mingw32-g++
else
    CC := g++
endif

STD_TAG=--std=c++14

CFLAGS= -Wall -g -DDEBUG

CC:=${CC} ${STD_TAG} ${CFLAGS}
LD_FLAGS=-lboost_filesystem \
         -lboost_system \
         -lboost_regex \
         -lcrypto

LOCAL_MODULE := TSDownload
LOCAL_SRCS := main.cpp \
    httpurl.cpp \
    TSDownloader.cpp \
    log.cpp

LOCAL_OBJS := $(patsubst %.cpp,%.o,${LOCAL_SRCS})

.PHONY: all

all: ${LOCAL_MODULE}

${LOCAL_MODULE}: ${LOCAL_OBJS}
	${CC} ${LIB_PATH} $^ -o $@ ${LD_FLAGS}

%.o: %.cpp
	${CC} -c $<

clean:
	${RM} *.o
	${RM} *.exe
