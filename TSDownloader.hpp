#ifndef TSDOWNLOADER_HPP
#define TSDOWNLOADER_HPP

#include <string>
#include <memory>
#include <vector>

class TSDownloader
{
    class TSDownloaderImpl;
public:
    TSDownloader(const std::string& url, const std::string& name);
    ~TSDownloader();

    int download();

private:
    std::unique_ptr<TSDownloaderImpl> impl;
};

#endif
