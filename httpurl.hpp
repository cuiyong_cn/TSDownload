#ifndef HTTPURL_H
#define HTTPURL_H

#include <string>

class HTTPURL
{
public:
    HTTPURL(std::string url);

    std::string original_url;
    std::string protocol;
    std::string domain;
    std::string port;
    std::string path;
    std::string query;
};

#endif /* HTTPURL_H */
